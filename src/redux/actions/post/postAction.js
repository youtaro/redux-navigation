import axios from 'axios';
import {baseUrl} from '../../../api/baseUrl';

export const getAllPost = () => {
  return async (dispatch) => {
    await axios.get(`${baseUrl}/posts`).then((res) => {
      dispatch({
        type: 'GET_ALL_POST',
        allPost: res.data,
      });
    });
  };
};

export const getPost = (id) => {
  return async (dispatch) => {
    await axios.get(`${baseUrl}/posts/${id}`).then((res) => {
      dispatch({
        type: 'GET_POST',
        post: res.data,
      });
    });
  };
};
