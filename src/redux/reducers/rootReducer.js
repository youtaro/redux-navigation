import {combineReducers} from 'redux';

import {postReducer} from '../reducers/post/postReducer';
const reducers = {
  postReducer: postReducer,
};

export const rootReducer = combineReducers(reducers);
