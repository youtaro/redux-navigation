const iniState = {
  allPost: null,
  post: null,
};

export const postReducer = (state = iniState, action) => {
  switch (action.type) {
    case 'GET_ALL_POST':
      return {
        ...state,
        allPost: action.allPost,
      };
    case 'GET_POST':
      return {
        ...state,
        post: action.post,
      };
    default:
      return state;
  }
};
