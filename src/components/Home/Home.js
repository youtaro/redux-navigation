import {H3, List, ListItem} from 'native-base';
import React, {Component} from 'react';
import {FlatList, SafeAreaView, Text, View} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {getAllPost, getPost} from '../../redux/actions/post/postAction';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allPost: null,
      post: null,
    };
  }

  componentDidMount() {
    this.props.getAllPost();
  }

  componentDidUpdate(prevProps) {
    const {allPost, post} = this.props;
    if (prevProps.allPost !== allPost) {
      this.setState({
        allPost,
      });
    }

    if (prevProps.post !== post) {
      this.setState({
        post,
      });
    }
  }

  render() {
    const {navigation} = this.props;
    const {allPost} = this.state;
    return (
      <SafeAreaView>
        <List>
          <FlatList
            data={allPost}
            keyExtractor={(item) => item.id.toString()}
            renderItem={({item}) => (
              <ListItem
                onPress={() =>
                  navigation.navigate('Post Card', {id: item?.id})
                }>
                <Text> {item?.title} </Text>
              </ListItem>
            )}
          />
        </List>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    allPost: state.postReducer.allPost,
    post: state.postReducer.post,
  };
};

const mapDispatchToProps = (dispatch) => {
  const boundActionCreators = bindActionCreators(
    {
      getAllPost,
      getPost,
    },
    dispatch,
  );
  return boundActionCreators;
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
