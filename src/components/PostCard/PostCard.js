import {Body, Card, CardItem, H3, List, ListItem} from 'native-base';
import React, {Component} from 'react';
import {SafeAreaView, Text, View} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {getPost} from '../../redux/actions/post/postAction';

class PostCard extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.props.getPost(this.props.route.params.id);
  }

  render() {
    const {post} = this.props;
    return (
      <SafeAreaView>
        <Card>
          <CardItem>
            <H3>{post?.title}</H3>
          </CardItem>
          <CardItem>
            <Text>{post?.body}</Text>
          </CardItem>
        </Card>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    post: state.postReducer.post,
  };
};

const mapDispatchToProps = (dispatch) => {
  const boundActionCreators = bindActionCreators(
    {
      getPost,
    },
    dispatch,
  );
  return boundActionCreators;
};

export default connect(mapStateToProps, mapDispatchToProps)(PostCard);
