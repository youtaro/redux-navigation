import React from 'react';
import {Provider} from 'react-redux';
import Home from './src/components/Home/Home';
import PostCard from './src/components/PostCard/PostCard';
import {store} from './src/redux/store/store';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

const stack = createStackNavigator();
export default function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <stack.Navigator initialRouteName="Home">
          <stack.Screen
            options={{
              headerStyle: {backgroundColor: '#F2CA05'},
              headerTintColor: '#ffffff',
            }}
            component={Home}
            name="Home"
          />
          <stack.Screen
            options={{
              headerStyle: {backgroundColor: '#F2CA05'},
              headerTintColor: '#ffffff',
            }}
            component={PostCard}
            name="Post Card"
          />
        </stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}
